HEADERS += \
        $$PWD/centerpopupbackgroundwidget.h \
        $$PWD/centerpopupwidget.h \
        $$PWD/commondefinition.h \
        $$PWD/notifymanager.h \
        $$PWD/notifyreceiveinfo.h \
        $$PWD/popupitemwidget.h \
        $$PWD/notifications_adaptor.h \
        $$PWD/adaptscreeninfo.h\
        $$PWD/notifications_interface.h \
        $$PWD/toptransparentwidget.h \
        $$PWD/sqlinfodata.h

SOURCES += \
        $$PWD/centerpopupbackgroundwidget.cpp \
        $$PWD/centerpopupwidget.cpp \
        $$PWD/notifymanager.cpp \
        $$PWD/notifyreceiveinfo.cpp \
        $$PWD/popupitemwidget.cpp \
        $$PWD/notifications_adaptor.cpp \
        $$PWD/adaptscreeninfo.cpp \
        $$PWD/notifications_interface.cpp \
        $$PWD/toptransparentwidget.cpp \
        $$PWD/sqlinfodata.cpp
