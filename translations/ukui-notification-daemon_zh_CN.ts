<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="82"/>
        <source>ukui-notification-daemon service</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>popupItemWidget</name>
    <message>
        <source>Please unlock to view</source>
        <translation type="vanished">请解锁查看</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="397"/>
        <source>From </source>
        <translation>来自</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="397"/>
        <source> one notification</source>
        <translation>的1条通知</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="424"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="861"/>
        <source>also</source>
        <translation>还有</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="861"/>
        <source>notice</source>
        <translation>条通知</translation>
    </message>
</context>
</TS>
