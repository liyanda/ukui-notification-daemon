<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="82"/>
        <source>ukui-notification-daemon service</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>popupItemWidget</name>
    <message>
        <source>Please unlock to view</source>
        <translation type="vanished">སྒྲག་འགྲོལ་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="397"/>
        <source>From </source>
        <translation>ནས།</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="397"/>
        <source> one notification</source>
        <translation>བརྡ་ཐོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="424"/>
        <source>Tips</source>
        <translation>གསལ་འདེབས་</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="861"/>
        <source>also</source>
        <translation>ད་དུང་</translation>
    </message>
    <message>
        <location filename="../src/popupitemwidget.cpp" line="861"/>
        <source>notice</source>
        <translation>བརྡ་ཐོ་</translation>
    </message>
</context>
</TS>
